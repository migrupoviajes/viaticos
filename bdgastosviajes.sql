USE [master]
GO
/****** Object:  Database [sistema_gastosviajes]    Script Date: 06/12/2022 10:51:19 a. m. ******/
CREATE DATABASE [sistema_gastosviajes]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'sistema_gastosviajes', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\sistema_gastosviajes.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'sistema_gastosviajes_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\sistema_gastosviajes_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [sistema_gastosviajes] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [sistema_gastosviajes].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [sistema_gastosviajes] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET ARITHABORT OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [sistema_gastosviajes] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [sistema_gastosviajes] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET  DISABLE_BROKER 
GO
ALTER DATABASE [sistema_gastosviajes] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [sistema_gastosviajes] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET RECOVERY FULL 
GO
ALTER DATABASE [sistema_gastosviajes] SET  MULTI_USER 
GO
ALTER DATABASE [sistema_gastosviajes] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [sistema_gastosviajes] SET DB_CHAINING OFF 
GO
ALTER DATABASE [sistema_gastosviajes] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [sistema_gastosviajes] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [sistema_gastosviajes] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [sistema_gastosviajes] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'sistema_gastosviajes', N'ON'
GO
ALTER DATABASE [sistema_gastosviajes] SET QUERY_STORE = OFF
GO
USE [sistema_gastosviajes]
GO
/****** Object:  Table [dbo].[centrodecostos]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[centrodecostos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_centrodecostos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[comprobante_degastos]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comprobante_degastos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[clavesolicitud] [int] NOT NULL,
	[clavecuentagasto] [int] NOT NULL,
	[imagen] [varbinary](max) NULL,
	[nombreimagen] [nvarchar](max) NULL,
	[precio] [decimal](18, 0) NOT NULL,
	[fechacaptura] [nvarchar](50) NOT NULL,
	[fechagasto] [nvarchar](50) NOT NULL,
	[comentario] [nvarchar](max) NOT NULL,
	[estatus] [nvarchar](50) NULL,
 CONSTRAINT [PK_comprobante_degastos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[contablesempleado]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contablesempleado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[monto] [int] NOT NULL,
	[clavecontable] [int] NOT NULL,
	[claveusuario] [int] NOT NULL,
	[clavesolicitud] [int] NOT NULL,
	[estatus] [nvarchar](50) NULL,
 CONSTRAINT [PK_contablesempleado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cuentagastos]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cuentagastos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_cuenta de gastos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cuentas_contables]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cuentas_contables](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
	[descripcion] [nvarchar](max) NOT NULL,
	[clave_gasto] [int] NOT NULL,
 CONSTRAINT [PK_cuentas_contables] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[formapago]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[formapago](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[formapago] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_formapago] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[proyecto]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[proyecto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
	[descripcion] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_proyecto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[solicitar_viaticos]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[solicitar_viaticos](
	[idsolicitud] [int] IDENTITY(1,1) NOT NULL,
	[nombreviatico] [nvarchar](50) NULL,
	[claveusuario] [int] NOT NULL,
	[claveproyecto] [int] NOT NULL,
	[fechasalida] [varchar](50) NOT NULL,
	[fechacaptura] [nvarchar](50) NOT NULL,
	[fecharegreso] [varchar](50) NOT NULL,
	[claveuniadmin] [int] NOT NULL,
	[clavecentrocostos] [int] NOT NULL,
	[comentarios] [varchar](max) NOT NULL,
	[origen] [nvarchar](50) NOT NULL,
	[destino] [nvarchar](50) NOT NULL,
	[estatus] [nvarchar](50) NOT NULL,
	[importe] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_solicitar_viaticos] PRIMARY KEY CLUSTERED 
(
	[idsolicitud] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[uniAdmin]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[uniAdmin](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_uniAdmin] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
	[usuario] [nvarchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[contraseña] [varchar](50) NOT NULL,
	[restablecer] [int] NULL,
	[fecha] [nvarchar](50) NOT NULL,
	[sexo] [nchar](10) NOT NULL,
	[role] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[comprobante_degastos]  WITH CHECK ADD  CONSTRAINT [FK_comprobante_degastos_cuentas_contables] FOREIGN KEY([clavecuentagasto])
REFERENCES [dbo].[cuentas_contables] ([id])
GO
ALTER TABLE [dbo].[comprobante_degastos] CHECK CONSTRAINT [FK_comprobante_degastos_cuentas_contables]
GO
ALTER TABLE [dbo].[comprobante_degastos]  WITH CHECK ADD  CONSTRAINT [FK_comprobante_degastos_solicitar_viaticos] FOREIGN KEY([clavesolicitud])
REFERENCES [dbo].[solicitar_viaticos] ([idsolicitud])
GO
ALTER TABLE [dbo].[comprobante_degastos] CHECK CONSTRAINT [FK_comprobante_degastos_solicitar_viaticos]
GO
ALTER TABLE [dbo].[contablesempleado]  WITH CHECK ADD  CONSTRAINT [FK_contablesempleado_cuentas_contables] FOREIGN KEY([clavecontable])
REFERENCES [dbo].[cuentas_contables] ([id])
GO
ALTER TABLE [dbo].[contablesempleado] CHECK CONSTRAINT [FK_contablesempleado_cuentas_contables]
GO
ALTER TABLE [dbo].[contablesempleado]  WITH CHECK ADD  CONSTRAINT [FK_contablesempleado_solicitar_viaticos] FOREIGN KEY([clavesolicitud])
REFERENCES [dbo].[solicitar_viaticos] ([idsolicitud])
GO
ALTER TABLE [dbo].[contablesempleado] CHECK CONSTRAINT [FK_contablesempleado_solicitar_viaticos]
GO
ALTER TABLE [dbo].[contablesempleado]  WITH CHECK ADD  CONSTRAINT [FK_contablesempleado_usuarios] FOREIGN KEY([claveusuario])
REFERENCES [dbo].[usuarios] ([id])
GO
ALTER TABLE [dbo].[contablesempleado] CHECK CONSTRAINT [FK_contablesempleado_usuarios]
GO
ALTER TABLE [dbo].[cuentas_contables]  WITH CHECK ADD  CONSTRAINT [FK_cuentas_contables_cuentagastos] FOREIGN KEY([clave_gasto])
REFERENCES [dbo].[cuentagastos] ([id])
GO
ALTER TABLE [dbo].[cuentas_contables] CHECK CONSTRAINT [FK_cuentas_contables_cuentagastos]
GO
ALTER TABLE [dbo].[solicitar_viaticos]  WITH CHECK ADD  CONSTRAINT [FK_solicitar_viaticos_centrodecostos] FOREIGN KEY([clavecentrocostos])
REFERENCES [dbo].[centrodecostos] ([id])
GO
ALTER TABLE [dbo].[solicitar_viaticos] CHECK CONSTRAINT [FK_solicitar_viaticos_centrodecostos]
GO
ALTER TABLE [dbo].[solicitar_viaticos]  WITH CHECK ADD  CONSTRAINT [FK_solicitar_viaticos_proyecto] FOREIGN KEY([claveproyecto])
REFERENCES [dbo].[proyecto] ([id])
GO
ALTER TABLE [dbo].[solicitar_viaticos] CHECK CONSTRAINT [FK_solicitar_viaticos_proyecto]
GO
ALTER TABLE [dbo].[solicitar_viaticos]  WITH CHECK ADD  CONSTRAINT [FK_solicitar_viaticos_uniAdmin] FOREIGN KEY([claveuniadmin])
REFERENCES [dbo].[uniAdmin] ([id])
GO
ALTER TABLE [dbo].[solicitar_viaticos] CHECK CONSTRAINT [FK_solicitar_viaticos_uniAdmin]
GO
ALTER TABLE [dbo].[solicitar_viaticos]  WITH CHECK ADD  CONSTRAINT [FK_solicitar_viaticos_usuarios] FOREIGN KEY([claveusuario])
REFERENCES [dbo].[usuarios] ([id])
GO
ALTER TABLE [dbo].[solicitar_viaticos] CHECK CONSTRAINT [FK_solicitar_viaticos_usuarios]
GO
/****** Object:  StoredProcedure [dbo].[cp_comprobantedegasto]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[cp_comprobantedegasto](
@clavesolicitud int,
@clavecuentagasto int,
@precio decimal(18,0),
@fechacaptura nvarchar(50),
@fechagasto nvarchar(50),
@comentario  nvarchar(max),
@estatus   nvarchar(50),


@Mensaje varchar(100) output,
@Resultado int output
) 
as 
begin 

SET @Resultado=0;

begin 
insert into comprobante_degastos(clavesolicitud,clavecuentagasto,precio,fechacaptura,fechagasto,
comentario,estatus) values (@clavesolicitud,@clavecuentagasto,@precio,
@fechacaptura,@fechagasto,@comentario,@estatus )
SET @Resultado= SCOPE_IDENTITY()
end 
end
GO
/****** Object:  StoredProcedure [dbo].[cp_editarcentrocostos]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[cp_editarcentrocostos](
@id int,
@nombre nvarchar(50),
@Mensaje varchar(100) output,
@Resultado bit output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM centrodecostos where nombre=@nombre and id!=@id)
begin 
update top(1)centrodecostos set 
nombre=@nombre
where id=@id
SET @Resultado= 1
end 
else 
set @Mensaje= 'elcentro de costo ya existe '
end 
GO
/****** Object:  StoredProcedure [dbo].[cp_editarformapago]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[cp_editarformapago](
@id int,
@formapago nvarchar(50),
@Mensaje varchar(100) output,
@Resultado bit output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM formapago where formapago=@formapago and id!=@id)
begin 
update top(1)formapago set 
formapago=@formapago

where id=@id
SET @Resultado= 1
end 
else 
set @Mensaje= 'el forma pago ya existe'
end 
GO
/****** Object:  StoredProcedure [dbo].[cp_editarproyecto]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[cp_editarproyecto](
@id int,
@nombre nvarchar(50),
@descripcion nvarchar(max),
@Mensaje varchar(100) output,
@Resultado bit output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM proyecto where nombre=@nombre and id!=@id)
begin 
update top(1)proyecto set 
nombre=@nombre,
descripcion=@descripcion
where id=@id
SET @Resultado= 1
end 
else 
set @Mensaje= 'el proyecto ya existe'
end 
GO
/****** Object:  StoredProcedure [dbo].[cp_eliminarcentrocosto]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[cp_eliminarcentrocosto](
@id int,
@Mensaje varchar(100) output,
@Resultado bit output
) 
as 
begin 

SET @Resultado=0;

begin 
delete top(1) from centrodecostos where id=@id 


SET @Resultado= 1
end 

end 


GO
/****** Object:  StoredProcedure [dbo].[cp_eliminarcuentagasto]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[cp_eliminarcuentagasto](
@id int,
@Mensaje varchar(100) output,
@Resultado bit output
) 
as 
begin 
SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM cuentas_contables cv
inner join cuentagastos cg on cg.id = cv.id
where cg.id =@id)
begin 
delete top(1) from cuentagastos where id=@id 
SET @Resultado= 1
end
else
set @Mensaje='el gasto se encuentra relacionaco con cuentas contables'

end 
GO
/****** Object:  StoredProcedure [dbo].[cp_eliminarformapago]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[cp_eliminarformapago](
@id int,
@Mensaje varchar(100) output,
@Resultado bit output
) 
as 
begin 

SET @Resultado=0;

begin 
delete top(1) from formapago where id=@id 


SET @Resultado= 1
end 

end 
GO
/****** Object:  StoredProcedure [dbo].[cp_eliminarproyecto]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[cp_eliminarproyecto](
@id int,
@Mensaje varchar(100) output,
@Resultado bit output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM solicitar_viaticos cv
inner join proyecto cg on cg.id = cv.claveproyecto
inner join comprobante_degastos cdg on cdg.claveproyecto=cg.id
inner join estatuscomprobante eg on eg.refgastoviaje=cg.id


where cg.id =@id)
begin 
delete top(1) from proyecto where id=@id 


SET @Resultado= 1
end 
else
set @Mensaje='el proyecto se encuentra relacionado'


end 
GO
/****** Object:  StoredProcedure [dbo].[cp_eliminarunidaadmin]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[cp_eliminarunidaadmin](
@id int,
@Mensaje varchar(100) output,
@Resultado bit output
) 
as 
begin 
SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM uniAdmin cv
inner join solicitar_viaticos cg on cg.claveuniadmin = cv.id
where cg.claveuniadmin =@id)
begin 
delete top(1) from uniAdmin where id=@id 
SET @Resultado= 1
end
else
set @Mensaje='la unidad admin esta relacionado con cuentas solictar viaticos'

end 
GO
/****** Object:  StoredProcedure [dbo].[cp_registraformapago]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[cp_registraformapago](
@formapago nvarchar(50),
@Mensaje varchar(100) output,
@Resultado int output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM formapago where formapago=@formapago)
begin 
insert into formapago(formapago) values (@formapago)
SET @Resultado= SCOPE_IDENTITY()
end 
else 
set @Mensaje= 'el forma pago existe'
end 
GO
/****** Object:  StoredProcedure [dbo].[cp_registraproyecto]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[cp_registraproyecto](
@nombre nvarchar(50),
@descripcion nvarchar(max),
@Mensaje varchar(100) output,
@Resultado int output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM proyecto where nombre=@nombre)
begin 
insert into proyecto (nombre,descripcion) values (@nombre,@descripcion)
SET @Resultado= SCOPE_IDENTITY()
end 
else 
set @Mensaje= 'el proyecto existe'
end 
GO
/****** Object:  StoredProcedure [dbo].[cp_registrarcentrocosto]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[cp_registrarcentrocosto](
@nombre nvarchar(50),
@Mensaje varchar(100) output,
@Resultado int output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT * FROM centrodecostos where nombre=@nombre)
begin 
insert into centrodecostos(nombre) values (@nombre)
SET @Resultado= SCOPE_IDENTITY()
end 
else 
set @Mensaje= 'el centro de costo existe'
end 
GO
/****** Object:  StoredProcedure [dbo].[cp_registrarcontaempleados]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[cp_registrarcontaempleados](
@monto float,
@clavecontable int,
@claveusuario int,
@clavesolicitud int,
@Mensaje varchar(100) output,
@Resultado int output
) 
as 
begin 

SET @Resultado=0;
select * from contablesempleado

begin 
insert into contablesempleado(monto,clavecontable,claveusuario,clavesolicitud) values (@monto,@clavecontable,@claveusuario,@clavesolicitud)
SET @Resultado= SCOPE_IDENTITY()
end 

end 

GO
/****** Object:  StoredProcedure [dbo].[cp_registrarcuencontables]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[cp_registrarcuencontables](
@nombre nvarchar(50),
@descripcion nvarchar(50),
@clave_gasto int,
@Mensaje varchar(100) output,
@Resultado int output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM cuentas_contables where nombre=@nombre)
begin 
insert into cuentas_contables(nombre,descripcion,clave_gasto) values (@nombre,@descripcion,@clave_gasto)
SET @Resultado= SCOPE_IDENTITY()
end 
else 
set @Mensaje= 'la cuenta contable existe'
end 
GO
/****** Object:  StoredProcedure [dbo].[cp_registrargasto]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[cp_registrargasto](
@nombre nvarchar(50),

@Mensaje varchar(100) output,
@Resultado int output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM cuentagastos where nombre=@nombre)
begin 
insert into cuentagastos(nombre) values (@nombre)
SET @Resultado= SCOPE_IDENTITY()
end 
else 
set @Mensaje= 'el gasto existe'
end 
GO
/****** Object:  StoredProcedure [dbo].[cp_registrarunidadadmin]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[cp_registrarunidadadmin](
@nombre nvarchar(50),
@Mensaje varchar(100) output,
@Resultado int output
) 
as 
begin 

SET @Resultado=0;
IF NOT EXISTS (SELECT* FROM uniAdmin where nombre=@nombre)
begin 
insert into uniAdmin(nombre) values (@nombre)
SET @Resultado= SCOPE_IDENTITY()
end 
else 
set @Mensaje= 'la unidad administrativa ya existe'
end 
GO
/****** Object:  StoredProcedure [dbo].[cp_registrarviaje]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[cp_registrarviaje](
@claveusuario int,
@nombreviatico  nvarchar(50),
@claveproyecto int,
@fechasalida varchar(50),
@fechacaptura varchar(50),
@fecharegreso varchar(50),
@claveuniadmin  int,
@clavecentrocosto int,
@comentario varchar(max),
@origen nvarchar(50),
@destino nvarchar(50),
@estatus nvarchar(50),
@importe decimal(18,0),
@Mensaje varchar(100) output,
@Resultado int output
) 
as 
begin 

SET @Resultado=0;

begin 
insert into solicitar_viaticos(claveusuario,nombreviatico,claveproyecto,fechasalida,fechacaptura,fecharegreso,
claveuniadmin,clavecentrocostos,comentarios,origen,destino,estatus,importe) values (@claveusuario,@nombreviatico,@claveproyecto,
@fechasalida,@fechacaptura,@fecharegreso,@claveuniadmin,@clavecentrocosto,@comentario,@origen,@destino,@estatus,@importe )
SET @Resultado= SCOPE_IDENTITY()
end 
end
GO
/****** Object:  StoredProcedure [dbo].[pr_editarusuario]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[pr_editarusuario](
@id int ,
@nombre nvarchar(50),
@usuario nvarchar(50),
@email varchar(50),
@fecha date,
@sexo nchar(10),
@role nvarchar(50),
@mensaje varchar(500) output,
@resultado bit output
)
as 
begin 
SET @resultado=0 
if NOT EXISTS (select * from usuarios where email=@email and id!=@id)
  begin 
 update top(1)usuarios SET 
 nombre=@nombre,
 usuario= @usuario,
 email=@email,
 fecha=@fecha,
 sexo=@role
 WHERE id=@id
 

  SET @resultado =1
  end
  else 
   SET @mensaje='el correo del usuario ya existe '
end 
GO
/****** Object:  StoredProcedure [dbo].[pr_registrarusuario]    Script Date: 06/12/2022 10:51:20 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[pr_registrarusuario](
@nombre nvarchar(50),
@usuario nvarchar(50),
@email varchar(50),
@contraseña varchar(50),
@fecha nvarchar(50),
@sexo nchar(10),
@role nvarchar(50),
@mensaje varchar(500) output,
@resultado int output
)
as 
begin 
SET @resultado=0 
if NOT EXISTS (select * from usuarios where email=@email)
  begin 
  insert into usuarios (nombre,usuario,email,contraseña,fecha,sexo,role) values 
  (@nombre, @usuario,@email,@contraseña,@fecha,@sexo,@role)
  SET @resultado =SCOPE_IDENTITY()
  end
  else 
   SET @mensaje='el correo ya existe '
end 
GO
USE [master]
GO
ALTER DATABASE [sistema_gastosviajes] SET  READ_WRITE 
GO
