﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data.SqlClient;
using System.Data;
using System.Web;




namespace backendDatos
{
    public class cr_comprobantegasto
    {
        

        public List<comprobante_gasto> Listarcomprobante(int id)
        {
            List<comprobante_gasto> lista = new List<comprobante_gasto>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    StringBuilder sw = new StringBuilder();
                    sw.AppendLine("select sc.id,sv.nombreviatico,scon.nombre,sc.precio,sc.nombreimagen,sc.fechacaptura,sc.fechagasto,sc.comentario,sc.estatus from solicitar_viaticos sv ");
                    sw.AppendLine("inner join comprobante_degastos sc on sv.idsolicitud=sc.clavesolicitud inner join cuentas_contables scon on scon.id=sc.clavecuentagasto where sc.clavesolicitud=@id");
                    
                    SqlCommand cmd = new SqlCommand(sw.ToString(), oconexion);
                    cmd.Parameters.AddWithValue("@id", id);

                    cmd.CommandType = CommandType.Text;
                    

                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista.Add(new comprobante_gasto()
                            {
                                id = Convert.ToInt16(dr["id"]),
                                clavesolictud = new solicitarviaje() {  nombreviatico = dr["nombreviatico"].ToString() },
                                clavecuentagasto = new cuentas_contables() { nombre = dr["nombre"].ToString() },
                                precio = Convert.ToDecimal(dr["precio"]),
                             //   rutaimagen= dr["rutaimagen"].ToString(),
                                nombreimagen = dr["nombreimagen"].ToString(),
                                fechacaptura = dr["fechacaptura"].ToString(),
                                fechagasto = dr["fechagasto"].ToString(),
                                comentario = dr["comentario"].ToString(),
                                estatus = dr["estatus"].ToString(),
                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<comprobante_gasto>();

            }


            return lista;

        }

        public List<comprobante_gasto> Listarimagen()
        {
            List<comprobante_gasto> lista = new List<comprobante_gasto>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    StringBuilder sw = new StringBuilder();
                    sw.AppendLine("select sc.id,sv.nombreviatico,scon.nombre,sc.precio,sc.imagen,sc.nombreimagen,sc.fechacaptura,sc.fechagasto,sc.comentario from solicitar_viaticos sv ");
                    sw.AppendLine("inner join comprobante_degastos sc on sv.idsolicitud=sc.clavesolicitud inner join cuentas_contables scon on scon.id=sc.clavecuentagasto ");

                    SqlCommand cmd = new SqlCommand(sw.ToString(), oconexion);


                    cmd.CommandType = CommandType.Text;


                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {


                        while (dr.Read())
                        {


                            lista.Add(new comprobante_gasto()
                            {
                                id = Convert.ToInt16(dr["id"]),
                                clavesolictud = new solicitarviaje() { nombreviatico = dr["nombreviatico"].ToString() },
                                clavecuentagasto = new cuentas_contables() { nombre = dr["nombre"].ToString() },
                                precio = Convert.ToDecimal(dr["precio"]),
                               // rutaimagen = dr["rutaimagen"].ToString(),
                                imagen =  dr["imagen"] as byte[],
                                nombreimagen = dr["nombreimagen"].ToString(),
                                fechacaptura = dr["fechacaptura"].ToString(),
                                fechagasto = dr["fechagasto"].ToString(),
                                comentario = dr["comentario"].ToString(),
                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<comprobante_gasto>();

            }


            return lista;

        }
        public bool editarcomprobante(comprobante_gasto obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top (1) comprobante_degastos set estatus='{0}' where id=@id", obj.estatus);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@estatus", obj.estatus);

                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }



        public List<solicitarviaje> Listarviajesusua(int id)
        {
            List<solicitarviaje> lista = new List<solicitarviaje>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    StringBuilder sw = new StringBuilder();
                    sw.AppendLine("select cv.idsolicitud, cu.id,cu.nombre, cp.nombre[nombreproyecto],cv.fechasalida,cv.fechacaptura,cv.fecharegreso,ca.nombre[nombreadmin],");
                    sw.AppendLine("cc.nombre[nombrecentro],cv.comentarios,cv.origen,cv.destino,cv.estatus,cv.importe  from usuarios cu inner join solicitar_viaticos cv");
                    sw.AppendLine("on cu.id=cv.claveusuario  inner join proyecto cp on cp.id=cv.claveproyecto inner join uniAdmin ca on ca.id=cv.claveuniadmin");
                    sw.AppendLine("inner join centrodecostos cc on cc.id=cv.clavecentrocostos where cu.id=@id;");
                    SqlCommand cmd = new SqlCommand(sw.ToString(), oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;

                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista.Add(new solicitarviaje()
                            {
                                idsolicitud = Convert.ToInt16(dr["idsolicitud"]),
                                nombreusuario = new usuarios() { id = Convert.ToInt16(dr["id"]), nombre = dr["nombre"].ToString() },
                                nombreproyecto = new proyecto() { nombre = dr["nombreproyecto"].ToString() },
                                fechasalida = dr["fechasalida"].ToString(),
                                fechacaptura = dr["fechacaptura"].ToString(),
                                fecharegreso = dr["fecharegreso"].ToString(),
                                nombreuniadmin = new uniAdmin() { nombre = dr["nombreadmin"].ToString() },
                                centrocosto = new centrodecostos() { nombre = dr["nombrecentro"].ToString() },
                                comentarios = dr["comentarios"].ToString(),
                                origen = dr["origen"].ToString(),
                                destino = dr["destino"].ToString(),
                                estatus = dr["estatus"].ToString(),
                                importe = Convert.ToDecimal(dr["importe"].ToString())

                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<solicitarviaje>();

            }


            return lista;

        }


        public bool Eliminarcomprobante(int id, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("delete top(1) comprobante_degastos where id=@id", oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }

        public bool guardardatosimagen(comprobante_gasto objeto, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;

            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update comprobante_degastos set imagen=@imagen, nombreimagen='{1}' where id=@id",objeto.imagen,objeto.nombreimagen);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", objeto.id);
                    //cmd.Parameters.AddWithValue("@rutaimagen", objeto.rutaimagen);
                    cmd.Parameters.AddWithValue("@imagen", objeto.imagen);
                    cmd.Parameters.AddWithValue("@nombreimagen", objeto.nombreimagen);
                    cmd.CommandType = CommandType.Text;




                    oconexion.Open();
                    if (cmd.ExecuteNonQuery()>0) {
                        resultado = true;
                    }
                    else
                    {
                        Mensaje = "no se pudo actualizar";
                    }
                  



                }


            }
            catch (Exception e)
            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;


        }
        public bool editarcomprobantegasto(comprobante_gasto obj, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    string query = String.Format("update top(1) comprobante_degastos set clavesolicitud='{0}', clavecuentagasto='{1}',precio='{2}',fechacaptura='{3}',fechagasto='{4}',comentario='{5}' where id =@id", obj.clavesolictud.idsolicitud, obj.clavecuentagasto.id,  obj.precio, obj.fechacaptura, obj.fechagasto, obj. comentario);
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@clavesolicitud", obj.clavesolictud.idsolicitud);
                    cmd.Parameters.AddWithValue("@clavecuentagasto", obj.clavecuentagasto.id);
                    cmd.Parameters.AddWithValue("@precio", obj.precio);
                    cmd.Parameters.AddWithValue("@fechacaptura", obj.fechacaptura);
                    cmd.Parameters.AddWithValue("@fechagasto", obj.fechagasto);
                    cmd.Parameters.AddWithValue("@comentario", obj.comentario);
                    
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }


        public int Registrarcomprobante(comprobante_gasto obj, out String Mensaje)
        {
            int idautogenerado = 0;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("cp_comprobantedegasto", oconexion);
                    cmd.Parameters.AddWithValue("clavesolicitud", obj.clavesolictud.idsolicitud);
                    cmd.Parameters.AddWithValue("clavecuentagasto", obj.clavecuentagasto.id);
                    cmd.Parameters.AddWithValue("precio", obj.precio);
                    cmd.Parameters.AddWithValue("fechacaptura", obj.fechacaptura);
                    cmd.Parameters.AddWithValue("fechagasto", obj.fechagasto);
                    cmd.Parameters.AddWithValue("comentario", obj.comentario);
                    cmd.Parameters.AddWithValue("estatus", obj.estatus);

                    cmd.Parameters.Add("resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();
                    idautogenerado = Convert.ToInt32(cmd.Parameters["resultado"].Value);
                    Mensaje = cmd.Parameters["mensaje"].Value.ToString();



                }


            }
            catch (Exception e)
            {
                idautogenerado = 0;
                Mensaje = e.Message;

            }
            return idautogenerado;

        }

        public total sumarcomprobantes(int id)
        {
            total lista = new total();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    String query = "select sum(precio) AS TOTAL from comprobante_degastos where clavesolicitud=@id";

                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;

                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista = new total()
                            {
                                totalcontable = Convert.ToDecimal(dr["TOTAL"])
                            };
                        }

                    }

                }
            }
            catch
            {
                lista = new total();

            }


            return lista;

        }
    }
}
