﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data.SqlClient;
using System.Data;

namespace backendDatos
{
    public class cr_usuarios
    {
        public List<usuarios> ListarUsuario()
        {
            List<usuarios> lista = new List<usuarios>();
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    String query = "select id,nombre,usuario,email,contraseña,fecha,restablecer,sexo,role from usuarios";
                    SqlCommand cmd = new SqlCommand(query, oconexion);
                    cmd.CommandType = CommandType.Text;

                    oconexion.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lista.Add(new usuarios()
                            {
                                id = Convert.ToInt16(dr["id"]),
                                nombre = dr["nombre"].ToString(),
                               usuario = dr["usuario"].ToString(),
                                email = dr["email"].ToString(),
                                contraseña = dr["contraseña"].ToString(),
                                fecha = dr["fecha"].ToString(),
                                restablecer = Convert.ToInt16(dr["restablecer"]),
                                role = dr["role"].ToString()


                            }
                            );
                        }

                    }

                }
            }
            catch
            {
                lista = new List<usuarios>();

            }


            return lista;

        }
        public int Registrar(usuarios obj, out String Mensaje)
        {
            int idautogenerado = 0;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("pr_registrarusuario", oconexion);
                    cmd.Parameters.AddWithValue("nombre", obj.nombre);
                    cmd.Parameters.AddWithValue("usuario", obj.usuario);
                    cmd.Parameters.AddWithValue("email", obj.email);
                    cmd.Parameters.AddWithValue("contraseña", obj.contraseña);
                    cmd.Parameters.AddWithValue("fecha", obj.fecha);
                    cmd.Parameters.AddWithValue("sexo", obj.sexo);
                    cmd.Parameters.AddWithValue("role", obj.role);

                    cmd.Parameters.Add("resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();
                    idautogenerado = Convert.ToInt32(cmd.Parameters["resultado"].Value);
                    Mensaje = cmd.Parameters["mensaje"].Value.ToString();



                }


            }
            catch (Exception e)
            {
                idautogenerado = 0;
                Mensaje = e.Message;

            }
            return idautogenerado;

        }

        public bool editarusuario(usuarios obj, out String Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("pr_editarusuario", oconexion);
                    cmd.Parameters.AddWithValue("id", obj.id);
                    cmd.Parameters.AddWithValue("nombre", obj.nombre);
                    cmd.Parameters.AddWithValue("usuario", obj.usuario);
                    cmd.Parameters.AddWithValue("email", obj.email);
                    cmd.Parameters.AddWithValue("fecha", obj.fecha);
                    cmd.Parameters.AddWithValue("sexo", obj.sexo);
                    cmd.Parameters.AddWithValue("role", obj.role);
                    cmd.Parameters.Add("resultado", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("mensaje", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    oconexion.Open();
                    cmd.ExecuteNonQuery();

                    resultado = Convert.ToBoolean(cmd.Parameters["resultado"].Value);
                    Mensaje = cmd.Parameters["mensaje"].Value.ToString();



                }


            }
            catch (Exception e)
            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;

        }
        public bool Elimarusuario(int id, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("delete top (1) from usuarios where id=@id", oconexion);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }catch(Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }
        public bool Cambiarclave(int idusuario,string nuevaclave,out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("update usuarios set contraseña=@nuevaclave, restablecer=0 where id=@id", oconexion);
                    cmd.Parameters.AddWithValue("@id", idusuario);
                    cmd.Parameters.AddWithValue("@nuevaclave", nuevaclave);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }
        public bool restablecercontraseña(int idusuario,string clave, out string Mensaje)
        {
            bool resultado = false;
            Mensaje = string.Empty;
            try
            {
                using (SqlConnection oconexion = new SqlConnection(Conexion.con))
                {
                    SqlCommand cmd = new SqlCommand("update usuarios set contraseña=@nuevaclave where id=@id", oconexion);
                    cmd.Parameters.AddWithValue("@id", idusuario);
                    cmd.Parameters.AddWithValue("@nuevaclave", clave);
                    cmd.CommandType = CommandType.Text;
                    oconexion.Open();
                    resultado = cmd.ExecuteNonQuery() > 0 ? true : false;

                }


            }
            catch (Exception e)

            {
                resultado = false;
                Mensaje = e.Message;

            }
            return resultado;
        }

    }

}

