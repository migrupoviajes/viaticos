﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using backendDatos;
using CapaEntidad;

namespace CapaEmpleado
{
    public class Cn_cuentascontables
    {
        private cr_cuentascontables cuentascontables = new cr_cuentascontables();
        public List<cuentas_contables> Listarcuentascontables()
        {

            return cuentascontables.listarcuentascontables();
        }

        public int Registrarcontables(cuentas_contables obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre  es vacio";

            } else if (string.IsNullOrEmpty(obj.descripcion) || string.IsNullOrWhiteSpace(obj.descripcion))
            {
                Mensaje = "la descripcion  es vacio";
            }
            else if (obj.ocuentagasto.idgasto == 0)
            {
                Mensaje = "La clave de gasto no puede ser vacio";
            }




            if (string.IsNullOrEmpty(Mensaje))
            {
                return cuentascontables.registrarcuentascontables(obj, out Mensaje);

            }
            else
            {
                return 0;



            }

        }

        public bool editarcuentascontables(cuentas_contables obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre  es vacio";

            }
            else if (string.IsNullOrEmpty(obj.descripcion) || string.IsNullOrWhiteSpace(obj.descripcion))
            {
                Mensaje = "la descripcion  es vacio";
            }
            else if (obj.ocuentagasto.idgasto == 0)
            {
                Mensaje = "La clave de gasto no puede ser vacio";
            }


            if (string.IsNullOrEmpty(Mensaje))
            {
                return cuentascontables.editarcuentascontables(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }
        public bool Elimarcuentascontables(int id, out string Mensaje)
        {
            return cuentascontables.Elimarcuentascontables (id, out Mensaje);
        }
    }
}
