﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using backendDatos;
using CapaEntidad;

namespace CapaEmpleado
{
    public class Cn_comprobantegasto
    {

        private cr_comprobantegasto objbackenddatos = new cr_comprobantegasto();



        public List<comprobante_gasto> Listacomprobante(int id)
        {

            return objbackenddatos.Listarcomprobante(id);
        }

        public List<comprobante_gasto> Listarimagen()
        {

            return objbackenddatos.Listarimagen();
        }

        public int Registrarcomgastos(comprobante_gasto obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.fechacaptura) || string.IsNullOrWhiteSpace(obj.fechacaptura))
            {
                Mensaje = "la fecha es vacio";

            }
            else if (string.IsNullOrEmpty(obj.fechagasto) || string.IsNullOrWhiteSpace(obj.fechagasto))
            {
                Mensaje = "la fecha gasto no debe ser vacio";

            }
            else if (string.IsNullOrEmpty(obj.comentario) || string.IsNullOrWhiteSpace(obj.comentario))
            {
                Mensaje = "el comentario no debe ser vacio";

            }



            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.Registrarcomprobante(obj, out Mensaje);

            }
            else
            {
                return 0;



            }

        }


        public bool editarcomprobantegasto(comprobante_gasto obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.fechacaptura) || string.IsNullOrWhiteSpace(obj.fechacaptura))
            {
                Mensaje = "la fecha es vacio";

            }
            else if (string.IsNullOrEmpty(obj.fechagasto) || string.IsNullOrWhiteSpace(obj.fechagasto))
            {
                Mensaje = "la fecha gasto no debe ser vacio";

            }
            else if (string.IsNullOrEmpty(obj.comentario) || string.IsNullOrWhiteSpace(obj.comentario))
            {
                Mensaje = "el comentario no debe ser vacio";

            }



            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editarcomprobantegasto(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public bool editarcomprobante(comprobante_gasto obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.estatus) || string.IsNullOrWhiteSpace(obj.estatus))
            {
                Mensaje = "el estatus es vacio";

            }



            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editarcomprobante(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public total sumarcompro(int id)
        {

            return objbackenddatos.sumarcomprobantes(id);
        }
        public bool Elimarcomprobantegas(int id, out string Mensaje)
        {
            return objbackenddatos.Eliminarcomprobante(id, out Mensaje);
        }

        public bool guardardatosimagen(comprobante_gasto objeto, out string Mensaje)
        {

            return objbackenddatos.guardardatosimagen(objeto, out Mensaje); 
        }
    }
}

