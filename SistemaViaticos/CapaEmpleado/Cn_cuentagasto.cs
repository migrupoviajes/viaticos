﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using backendDatos;
using CapaEntidad;

namespace CapaEmpleado
{
    public class Cn_cuentagasto
    {

        private cr_cuentagastos objbackenddatos = new cr_cuentagastos();

        

        public List<cuentagastos> Listargastos()
        {

            return objbackenddatos.listargastos();
        }

        public int  Registrargastos(cuentagastos obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre  es vacio";

            }
            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.registrargastos(obj, out Mensaje);

            }
            else
            {
                return 0;
                    


            }

        }

        public bool editargasto(cuentagastos obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el nombre  es vacio";

            }


            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editargastos(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }
        public bool Elimargasto(int id, out string Mensaje)
        {
            return objbackenddatos.eliminargastos(id, out Mensaje);
        }
    }
}
