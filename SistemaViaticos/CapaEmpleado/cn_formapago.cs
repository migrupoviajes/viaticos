﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using backendDatos;
using CapaEntidad;

namespace CapaEmpleado
{
  public class cn_formapago
    {
        private cr_formapago objbackenddatos = new cr_formapago();

        public List<formapa> Listarformapago()
        {

            return objbackenddatos.Listarformapago();
        }

        public int Registrarformapago(formapa obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.formapago) || string.IsNullOrWhiteSpace(obj.formapago))
            {
                Mensaje = "el forma pago es vacio";

            }
          


            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.Registrarformapago(obj, out Mensaje);

            }
            else
            {
                return 0;


            }

        }

        public bool editarformapago(formapa obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.formapago) || string.IsNullOrWhiteSpace(obj.formapago))
            {
                Mensaje = "el nombre del proyecto es vacio";

            }
            

            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editarformapago(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }
        public bool Elimarformapago(int id, out string Mensaje)
        {
            return objbackenddatos.eliminarformapago(id, out Mensaje);
        }
    }
}
