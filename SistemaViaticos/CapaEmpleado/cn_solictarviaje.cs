﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using backendDatos;
using CapaEntidad;


namespace CapaEmpleado
{
   public  class cn_solictarviaje
    {
        private cr_solictarviaje objbackenddatos = new cr_solictarviaje();

        public List<solicitarviaje> Listar2()
        {

            return objbackenddatos.Listar();
        }
        public List<solicitarviaje> Listarporviaje(int id)
        {

            return objbackenddatos.Listarviaticos(id);
        }



        public List<solicitarviaje> Listarporusua(int id)
        {

            return objbackenddatos.Listarviajesusua(id);
        }
        public List<solicitarviaje> Listarporfecha(int id,string fecha1,string fecha2 )
        {

            return objbackenddatos.Listarviajeporfecha(id,fecha1,fecha2);
        }

        public int Registrar(solicitarviaje obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.fechasalida) || string.IsNullOrWhiteSpace(obj.fechasalida))
            {
                Mensaje = "la fecha de salida es vacio";

            }
            else
            if (string.IsNullOrEmpty(obj.fecharegreso) || string.IsNullOrWhiteSpace(obj.fecharegreso))
            {
                Mensaje = "la fecha de regreso es vacio";

            }
            else
            if (string.IsNullOrEmpty(obj.fechacaptura) || string.IsNullOrWhiteSpace(obj.fechacaptura))
            {
                Mensaje = "la fecha de captura es vacio";

            }
            else

            if (string.IsNullOrEmpty(obj.comentarios) || string.IsNullOrWhiteSpace(obj.comentarios))
            {
                Mensaje = "los comentario son vacio";

            }
            else if (string.IsNullOrEmpty(obj.origen) || string.IsNullOrWhiteSpace(obj.origen))
            {
                Mensaje = "el origen es vacio";
            }

            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.Registrarviaje(obj, out Mensaje);

            }
            else
            {
                return 0;


            }


        }

        public bool editarsolicitarviajes(solicitarviaje obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.fechasalida) || string.IsNullOrWhiteSpace(obj.fechasalida))
            {
                Mensaje ="la fecha de salida esta vacio";

            }else if (string.IsNullOrEmpty(obj.fechacaptura) || string.IsNullOrWhiteSpace(obj.fechacaptura))
            {
                Mensaje = "la fecha de captura es vacio";
            }
            else if (string.IsNullOrEmpty(obj.fecharegreso) || string.IsNullOrWhiteSpace(obj.fecharegreso))
            {
                Mensaje ="la fecha de regreso es vacio";

            }
            else if (string.IsNullOrEmpty(obj.comentarios) || string.IsNullOrWhiteSpace(obj.comentarios))
            {
                Mensaje = "los comentarios son vacios";

            }



            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editarsolicitudviaje(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public bool editaimpo(solicitarviaje obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (obj.importe==0)
            {
                Mensaje = "el importe es vacio";

            }



            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editarimporte(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public bool editaviajesito(solicitarviaje obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.estatus) || string.IsNullOrWhiteSpace(obj.estatus))
            {
                Mensaje = "el estatus es vacio";

            }



            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.editarviaje(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public bool bajaviaje(solicitarviaje obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (obj.idsolicitud==0)
            {
                Mensaje = "se encuentra vacio el idsolictud";

            }



            if (string.IsNullOrEmpty(Mensaje))
            {
                return objbackenddatos.dardebaja(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }
    }
}
